Directory that contains various assets, data and images to run `LivePose` tests.

# Filters

##Triangulation
`triangulate_cam1.png` and `triangulate_cam2.png` are the reference images.
The 2D points used in the test `filters/triangulate.py` are derived from these images.
The file, `triangulate_filter.blend` is the scene from which the 3D points are obtained.

# Deep learning backend

## PoseNet
The file `mosaico.png` is used to test PoseNet backend in `pose_backends/test_posenet.py`.
The image comes from a collaboration with the artistic collective formed by
choreographer Audrey Gaussiran and digital artist Francis Lecavalier.
This artistic residency, named Mosaico, explored how data captured by LivePose
can be used to modify and control visual effects generated and projected live using TouchDesigner.
