"""Unit tests for livepose/outputs/websocket.py"""

import json
from unittest import TestCase
import time

import asyncio
import threading
import numpy as np
import websockets

from livepose.outputs.websocket import WebsocketOutput


class TestWebsocketOutput(TestCase):
    """Test WebsocketOutput constructor."""

    async def client(self, destination, port):
        """Websocket client code. Keeps trying to connect to the provided
        address forever. Can be wrapped with `asyncio.wait_for()` to add a
        timeout for trying to connect.
        """
        uri = f"ws://{destination}:{port}"
        connected = False
        # Try to connect until websocket server is running.
        while not connected:
            try:
                async with websockets.connect(uri) as websocket:
                    connected = True
                    self.results = json.loads(await websocket.recv())
            except OSError:
                pass

    def test_server_start_shutdown(self):
        """Test that when a WebsocketOutput object is created, a websocket
        server begins running in the background, and can be shutdown.
        """
        connection = {
            "host": "127.0.0.1",
            "port": 8765
        }

        output = WebsocketOutput(server=connection)
        output.init()

        output.wait_for_server_start()

        self.assertTrue(output._server_thread.is_alive())

        # Server is running now.
        output.stop()

        self.assertTrue(output._stop)

        output.wait_for_server_stop()

        # Check that server thread has closed.
        self.assertFalse(output._server_thread.is_alive())

    def test_no_results(self):
        """Test websocket server is successfully created when a single
        destination is provided and no filter results are set.
        """
        connection = {
            "host": "127.0.0.1",
            "port": 8767
        }

        output = WebsocketOutput(server=connection)
        output.init()

        output.wait_for_server_start()

        self.results = None

        client = self.client(connection["host"], connection["port"])
        asyncio.get_event_loop().run_until_complete(client)

        self.assertEqual(self.results, {})

        output.stop()

        output.wait_for_server_stop()

    def test_with_results(self):
        """Test websocket server is successfully created and filter results are
        continually sent out correctly when filter results are set.
        """
        connection = {
            "host": "127.0.0.1",
            "port": 8769
        }

        output = WebsocketOutput(server=connection)
        output.init()

        output._filter_results["dummy_filter"] = [1., 2., 3., 5.]

        self.results = None

        client = self.client(connection["host"], connection["port"])
        asyncio.get_event_loop().run_until_complete(client)

        self.assertEqual(self.results, output._filter_results)

        output.stop()

        output.wait_for_server_stop()
