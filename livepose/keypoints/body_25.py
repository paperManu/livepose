from typing import Any, Dict

"""
Skeleton keypoints based on BODY_25 dataset.
"""
Body25Keypoints: Dict[str, Any] = {
    "NOSE": 0,
    "NECK": 1,
    "RIGHT_SHOULDER": 2,
    "RIGHT_ELBOW": 3,
    "RIGHT_WRIST": 4,
    "LEFT_SHOULDER": 5,
    "LEFT_ELBOW": 6,
    "LEFT_WRIST": 7,
    "MID_HIP": 8,
    "RIGHT_HIP": 9,
    "RIGHT_KNEE": 10,
    "RIGHT_ANKLE": 11,
    "LEFT_HIP": 12,
    "LEFT_KNEE": 13,
    "LEFT_ANKLE": 14,
    "RIGHT_EYE": 15,
    "LEFT_EYE": 16,
    "RIGHT_EAR": 17,
    "LEFT_EAR": 18,
    "LEFT_BIG_TOE": 19,
    "LEFT_SMALL_TOE": 20,
    "LEFT_HEEL": 21,
    "RIGHT_BIG_TOE": 22,
    "RIGHT_SMALL_TOE": 23,
    "RIGHT_HEEL": 24,
}
