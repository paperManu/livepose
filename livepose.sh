#!/bin/bash

# Run ./bin/livepose locally without installing LivePose package.

PROJECT_DIR="$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export PYTHONPATH=$PYTHONPATH:${PROJECT_DIR}

./bin/livepose "$@"
