import cv2
import logging
import json
import math

from typing import Any, Dict, List, Optional, Tuple

from livepose.camera import Camera

logger = logging.getLogger(__name__)


class SplashCalibrate:
    def __init__(self, planar: bool, keypoints_3d: List[List[float]], cams_info: Dict[str, Any]) -> None:
        self._calibrated: bool = False
        self._proj_error: float = math.inf
        self._planar: bool = planar

        # initialize the configuration
        self._keypoints_3d: List[List[float]] = keypoints_3d
        self._cam_dict: Dict[str, Camera] = {}
        self._2d_keypoints_dict: Dict[str, List[Tuple[int, int]]] = {}

        # loop over the cameras and initialize some attributes
        for name, info in cams_info.items():
            self._2d_keypoints_dict[name] = []
            self._cam_dict[name] = Camera(info["path"])
            if "camera_matrix" in info:
                self._cam_dict[name].kmat = info["camera_matrix"]
                self._cam_dict[name].dist_coeffs = info["dist_coeffs"]

    def update(self, name: str) -> None:
        self._cam_dict[name].grab()
        self._cam_dict[name].retrieve()

        if self._cam_dict[name].frame is None:
            return

        if self._2d_keypoints_dict[name]:
            for keypoint in self._2d_keypoints_dict[name]:
                cv2.drawMarker(self._cam_dict[name].frame, keypoint, (0, 0, 255),
                               markerType=cv2.MARKER_CROSS, markerSize=20, thickness=1, line_type=cv2.LINE_AA)

    def init_camera(self, name: str) -> bool:
        if not self._cam_dict[name].is_open():
            logger.error(f"Camera could not be open via path: {self._cam_dict[name].path}")
            return False

        return self._cam_dict[name].grab() and self._cam_dict[name].retrieve()

    def save2file(self, name: str, path: str) -> None:
        assert(self._cam_dict[name] is not None)
        assert(self._cam_dict[name].frame is not None)
        assert(self._cam_dict[name].kmat is not None)
        assert(self._cam_dict[name].dist_coeffs is not None)
        assert(self._cam_dict[name].extrinsics is not None)

        # save the results to file
        camera = self._cam_dict[name]
        assert(camera.kmat is not None)

        cam_data = {
            "proj_error": self._proj_error,
            "camera_matrix": camera.kmat.flatten().tolist(),
            "distortion_coeffs": camera.dist_coeffs.flatten().tolist(),
            "extrinsics": camera.extrinsics.flatten().tolist(),
            "2d points": self._2d_keypoints_dict[name],
            "3d points": self._keypoints_3d,
            # we want to be compatible with OpenCV matrix notations
            # frame_resolution is thus saved as [src.cols, src.rows]
            "frame_resolution": [camera.frame.shape[1], camera.frame.shape[0]]
        }

        with open(path, "w", encoding="utf-8") as f:
            json.dump(cam_data, f, ensure_ascii=False, indent=4)
